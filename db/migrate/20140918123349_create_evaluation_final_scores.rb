class CreateEvaluationFinalScores < ActiveRecord::Migration
  def self.up
    create_table :evaluation_final_scores do |t|
      t.integer :student_id
      t.integer :subject_id
      t.integer :total
      t.decimal :scale_grade_point,:precision =>15, :scale => 2
      t.decimal :scale_total,:precision =>15, :scale => 2
      t.decimal :scale_total_difference,:precision =>15, :scale => 2
      t.decimal :scale_credit_point,:precision =>15, :scale => 2
      t.decimal :scale_minimal_point,:precision =>15, :scale => 2
      t.decimal :processed_grade_point,:precision =>15, :scale => 2
      t.decimal :grade_point,:precision =>15, :scale => 2
      t.string  :grade
      t.timestamps
    end
  end

  def self.down
    drop_table :evaluation_final_scores
  end
end
