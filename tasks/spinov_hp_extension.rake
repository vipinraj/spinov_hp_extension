namespace :spinov_hp_extension do
  desc "Install Spinov Hp Extension Module"
  task :install do
    system "rsync -ruv --exclude=.svn vendor/plugins/spinov_hp_extension/public ."
  end
  
end
