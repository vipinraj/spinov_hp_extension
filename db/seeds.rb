menu_link_present = MenuLink rescue false
unless menu_link_present == false
  administration = MenuLinkCategory.find_by_name("administration")
  settings = MenuLink.find_by_name("settings")
  MenuLink.create(:name=>'manage_programme_groups',:target_controller=>'programme_groups',:target_action=>'index',:higher_link_id=>settings.id,:link_type=>'general',:user_type=>nil,:menu_link_category_id=>administration.id)  unless MenuLink.exists?(:name=>'manage_programme_groups')
end





tag = PrivilegeTag.find_or_create_by_name_tag(:name_tag=>'management',:priority=> 0)
Privilege.find_or_create_by_name(:name => 'CIO', :description => "cio").update_attributes(:privilege_tag_id=>tag.id, :priority=>411 )
Privilege.find_or_create_by_name(:name => 'Registrar', :description => "registrar").update_attributes(:privilege_tag_id=>tag.id, :priority=>422 )
Privilege.find_or_create_by_name(:name => 'AssistantRegistrar', :description => "asistant_registrar").update_attributes(:privilege_tag_id=>tag.id, :priority=>422 )

academics_tag = PrivilegeTag.find_by_name_tag('academics')
Privilege.find_or_create_by_name(:name => 'DeanAcademics', :description => "dean_academics").update_attributes(:privilege_tag_id=>academics_tag.id, :priority=>0 )
