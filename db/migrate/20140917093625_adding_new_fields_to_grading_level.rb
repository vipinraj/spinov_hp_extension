class AddingNewFieldsToGradingLevel < ActiveRecord::Migration
  def self.up
    add_column :grading_levels,:grade_point,:decimal, :precision =>15, :scale => 2
    add_column :grading_levels,:minimal_point,:decimal, :precision =>15, :scale => 2
  end

  def self.down
  end
end
