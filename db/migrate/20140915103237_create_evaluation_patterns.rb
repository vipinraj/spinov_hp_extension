class CreateEvaluationPatterns < ActiveRecord::Migration
  def self.up
    create_table :evaluation_patterns do |t|
    	t.integer :subject_id
    	t.string  :name
    	t.integer :weightage
    	t.integer :max_marks

      t.timestamps
    end
  end

  def self.down
    drop_table :evaluation_patterns
  end
end
