class AddColumnToEvaluationFinalScore < ActiveRecord::Migration
  def self.up
    add_column :evaluation_final_scores,:batch_id,:integer
    add_column :evaluation_final_scores,:school_id,:integer
    add_column :evaluation_scores,:school_id,:integer
    add_column :evaluation_patterns,:school_id,:integer
    
  end

  def self.down
  end
end
