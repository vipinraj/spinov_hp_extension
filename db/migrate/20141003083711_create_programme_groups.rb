class CreateProgrammeGroups < ActiveRecord::Migration
  def self.up
    create_table :programme_groups do |t|
      t.string :name
      t.string :code
      t.boolean:is_deleted,:default=>0
      t.integer:school_id
      t.timestamps
    end
  end

  def self.down
    drop_table :programme_groups
  end
end
