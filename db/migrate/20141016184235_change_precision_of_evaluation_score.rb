class ChangePrecisionOfEvaluationScore < ActiveRecord::Migration
  def self.up
    change_column :evaluation_scores,:weightage,:decimal,:precision =>15, :scale => 2
    change_column :evaluation_final_scores,:total,:decimal,:precision =>15, :scale => 2
  end

  def self.down
  end
end
