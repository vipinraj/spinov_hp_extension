module SpinovHpExtension
  module SubjectsController
      def self.included (base)
      base.instance_eval do
        alias_method :new,:patched_new
        alias_method :edit ,:patched_edit

      end
    end

    def patched_new
     @subject = Subject.new
    @batch = Batch.find params[:id] if request.xhr? and params[:id]
    @elective_group = ElectiveGroup.find params[:id2] unless params[:id2].nil?
    respond_to do |format|
      format.js { render :action => 'patched_new' }
    end
    end


    def patched_edit
    @subject = Subject.find params[:id]
    @batch = @subject.batch
    @elective_group = ElectiveGroup.find params[:id2] unless params[:id2].nil?
    respond_to do |format|
      format.html { }
      format.js { render :action => 'patched_edit' }
    end
  end


  end
  end