class CreateWorkGroups < ActiveRecord::Migration
  def self.up
    create_table :work_groups do |t|
      t.references :subject

      t.timestamps
    end
  end

  def self.down
    drop_table :work_groups
  end
end
