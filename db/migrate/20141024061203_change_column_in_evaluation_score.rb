class ChangeColumnInEvaluationScore < ActiveRecord::Migration
  def self.up
    change_column :evaluation_scores,:score,:decimal,:precision =>15, :scale => 2
  end

  def self.down
  end
end
