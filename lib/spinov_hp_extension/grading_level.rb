module SpinovHpExtension
module GradingLevelExt
    def self.included (base)
      base.instance_eval {extend ClassMethods}
      base.instance_eval do
        validates_presence_of :grade_point
        validates_presence_of :minimal_point

      end
    end

  module ClassMethods
       def grade_point_to_grade(grade_point, batch_id)
      batch_grades = GradingLevel.for_batch(batch_id)
      if batch_grades.empty?
        grade = GradingLevel.default.find :first,
          :conditions => [ "grade_point <= ?", grade_point.round ], :order => 'grade_point desc'
      else
        grade = GradingLevel.for_batch(batch_id).find :first,
          :conditions => [ "grade_point <= ?", grade_point.round ], :order => 'grade_point desc'
      end
      grade
    end
  end

end
  end