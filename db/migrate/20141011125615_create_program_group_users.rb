class CreateProgramGroupUsers < ActiveRecord::Migration
  def self.up
    create_table :program_group_users do |t|
      t.references :user
      t.references :programme_group

      t.timestamps
    end
  end

  def self.down
    drop_table :program_group_users
  end
end
