require 'translator'
require File.join(File.dirname(__FILE__), "lib", "spinov_hp_extension")
require 'dispatcher'

FedenaPlugin.register = {
  :name=>"spinov_hp_extension",
  :description=>"Spinov Hp Extension",
  :auth_file=>"config/spinov_hp_extension_auth.rb"
  #:multischool_models=>%w{EvaluationFinalScore EvaluationPattern EvaluationScore ProgrammeGroup StudentGroup StudentGroupStudent}
  #:finance=>{:category_name=>"applicant registration"}
}
Dir[File.join("#{File.dirname(__FILE__)}/config/locales/*.yml")].each do |locale|
  I18n.load_path.unshift(locale)
end

SpinovHpExtension.attach_overrides


     

