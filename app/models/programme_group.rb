class ProgrammeGroup < ActiveRecord::Base
  named_scope :active,{ :conditions => { :is_deleted => false }}
  has_many :courses
  validates_uniqueness_of :code,:scope=> [:is_deleted],:if=> 'is_deleted == false', :allow_blank => true
  validates_uniqueness_of :name,:scope=> [:is_deleted],:if=> 'is_deleted == false'
  validates_presence_of :name




  def self.management_roles
    [:cio,:registrar,:assistant_registrar]
  end

end
