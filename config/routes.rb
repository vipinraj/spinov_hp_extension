ActionController::Routing::Routes.draw do |map|
      map.resources :programme_groups,:collection=>{:manage_programs=>[:get]}
      map.resources :work_groups,  :collection => {:manage_students=>[:get,:post]}
end