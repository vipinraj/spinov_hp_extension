module SpinovHpExtension
module StudentExt

     def self.included (base)

      base.instance_eval do
        has_many :work_group_students
        has_many :work_groups, :through=> :work_group_students

      end
    end

    def get_batch_transcript(current_batch)
      @subjects = current_batch.subjects
      subject_array = [ ]
      subjects.each do | sub |
      evaluation_final_score = EvaluationFinalScore.find_by_student_id_and_subject_id(self.id,sub.id)
      grade_point = evaluation_final_score.grade_point if evaluation_final_score.present?
      grade = evaluation_final_score.grade if evaluation_final_score.present?
      subject_array << { :subject => sub, :grade_point => grade_point, :grade => grade }
      end
      return subject_array
  end
end
  end