class CreateWorkGroupStudents < ActiveRecord::Migration
  def self.up
    create_table :work_group_students do |t|
      t.references :student
      t.references :work_group
      t.timestamps
    end
  end

  def self.down
    drop_table :work_group_students
  end
end
