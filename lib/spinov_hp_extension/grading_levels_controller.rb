module SpinovHpExtension
  module GradingLevelsController
    def self.included (base)
      base.instance_eval do
        alias_method :update,:patched_update
        alias_method :create,:patched_create
        alias_method :index,:patched_index
        alias_method :show,:patched_show
        alias_method :new,:patched_new
        alias_method :edit,:patched_edit
      end
    end

    def patched_new
    @grading_level = GradingLevel.new
    @batch = Batch.find params[:id] if request.xhr? and params[:id]
    if @batch.present?
      @credit = @batch.gpa_enabled? || @batch.cce_enabled?
    else
      @credit = Configuration.cce_enabled? || Configuration.get_config_value('CWA')=='1' || Configuration.get_config_value('GPA')=='1'
    end
    respond_to do |format|
      format.js { render :action => 'patched_new' }
    end
      end



  def patched_edit
    @grading_level = GradingLevel.find params[:id]
    @batch = Batch.find(@grading_level.batch_id) unless @grading_level.batch_id.nil?
    if @batch.present?
      @credit = @batch.gpa_enabled? || @batch.cce_enabled?
    else
      @credit = Configuration.get_config_value('CCE')=='1' || Configuration.get_config_value('CWA')=='1' || Configuration.get_config_value('GPA')=='1'
    end
    respond_to do |format|
      format.html { }
      format.js { render :action => 'patched_edit' }
    end
  end

  def patched_show
    @batch = nil
    if params[:batch_id] == ''
      @grading_levels = GradingLevel.default
    else
      @grading_levels = GradingLevel.for_batch(params[:batch_id])
      @batch = Batch.find params[:batch_id] unless params[:batch_id] == ''
    end
    respond_to do |format|
      format.js { render :action => 'patched_show' }
    end
  end

    def patched_index
      @batches = Batch.active
      @grading_levels = GradingLevel.default
      render :template=>"grading_levels/patched_index"
    end

  def patched_create
    @grading_level = GradingLevel.new(params[:grading_level])
    @batch = Batch.find(params[:grading_level][:batch_id]) if params[:grading_level].present? && params[:grading_level][:batch_id].present?
    respond_to do |format|
      if @grading_level.save
        @grading_level.batch.nil? ?
            @grading_levels = GradingLevel.default :
            @grading_levels = GradingLevel.for_batch(@grading_level.batch_id)
        #flash[:notice] = 'Grading level was successfully created.'
        format.html { redirect_to grading_level_url(@grading_level) }
        format.js { render :action => 'patched_create' }
      else
        @error = true
        format.html { render :action => "patched_new" }
        format.js { render :action => 'patched_create' }
      end
    end
  end

    def patched_update
    @grading_level = GradingLevel.find params[:id]
    respond_to do |format|
      if @grading_level.update_attributes(params[:grading_level])
        if @grading_level.batch.nil?
          @grading_levels = GradingLevel.default
        else
          @batch = @grading_level.batch
          @grading_levels = GradingLevel.for_batch(@grading_level.batch_id)
        end
        format.html { redirect_to grading_level_url(@grading_level) }
        format.js { render :action => 'patched_update' }
      else
        @error = true
        format.html { render :action => "patched_new" }
        format.js { render :action => 'patched_create' }
      end
    end
    end


    end
  end