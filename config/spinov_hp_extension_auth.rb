authorization do
  role :teacher do
    has_permission_on [:evaluation_patterns],
                      :to=>[:new]
    has_permission_on [:student_groups],
                      :to=>[:new]
    has_permission_on [:evaluation_scores],
                      :to=>[:enter_score,:generate_report,:evaluation_final_score_report,:evaluation_final_score_pdf]
  end
  role :admin do
    includes :teacher
  end

  role :employee do
    includes :teacher
  end

  role :programme_group_manager do
    has_permission_on [:programme_groups],
                      :to=>[:new,:edit,:index,:manage_programs]
  end
  role :admin do
    includes :programme_group_manager
  end

  role :manage_course_batch do
    #includes :programme_group_manager
  end


  role :cio do
    includes :admin
    has_permission_on [:evaluation_patterns],
                      :to=>[:new]
  end


  role :registrar do
    includes :admin
    has_permission_on [:evaluation_patterns],
                      :to=>[:new]
  end


  role :assistant_registrar do
    includes :admin
    has_permission_on [:evaluation_patterns],
                      :to=>[:new]
  end

  role :dean_academics do

    includes :examination_control
    includes :enter_result
    includes :view_result
    includes :manage_timetable
    includes :timetable_view
    includes :manage_program_batch
    includes :course_master

    has_permission_on [:evaluation_patterns],
                      :to=>[:new]
     has_permission_on [:student_groups],
                      :to=>[:new]

    has_permission_on [:employee],
                      :to => [:subject_assignment,:update_subjects,:select_department,:update_employees,:remove_employee]

  end

end