module SpinovHpExtension
module BatchExt

     def self.included (base)

      base.instance_eval do

        named_scope :active_with_spinov,{ :joins=>:course,:conditions => ["batches.is_deleted = 0 AND batches.is_active = 1 AND courses.programme_group_id in (?)",Fedena.present_user.present? ? Fedena.present_user.valid_program_group_ids : ProgrammeGroup.active.collect(&:id)] ,:select=>"`batches`.*,CONCAT(courses.code,'-',batches.name) as course_full_name",:order=>"course_full_name",:include=>:course}
        alias_method :active, :active_with_spinov
      end
    end


end
  end