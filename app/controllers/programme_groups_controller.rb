class ProgrammeGroupsController < ApplicationController
  # GET /programme_groups
  # GET /programme_groups.xml
  def index
    @programme_groups = ProgrammeGroup.active.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @programme_groups }
    end
  end

  # GET /programme_groups/1
  # GET /programme_groups/1.xml
  def show
    @programme_group = ProgrammeGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @programme_group }
    end
  end

  # GET /programme_groups/new
  # GET /programme_groups/new.xml
  def new
    @programme_group = ProgrammeGroup.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @programme_group }
    end
  end

  # GET /programme_groups/1/edit
  def edit
    @programme_group = ProgrammeGroup.find(params[:id])
  end

  # POST /programme_groups
  # POST /programme_groups.xml
  def create
    @programme_group = ProgrammeGroup.new(params[:programme_group])

    respond_to do |format|
      if @programme_group.save
        flash[:notice] = 'Action successful'
        format.html { redirect_to(:action=>"index") }
        format.xml  { render :xml => @programme_group, :status => :created, :location => @programme_group }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @programme_group.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /programme_groups/1
  # PUT /programme_groups/1.xml
  def update
    @programme_group = ProgrammeGroup.find(params[:id])

    respond_to do |format|
      if @programme_group.update_attributes(params[:programme_group])
        flash[:notice] = 'Action successful'
        format.html { redirect_to(:action=>"index") }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @programme_group.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /programme_groups/1
  # DELETE /programme_groups/1.xml
  def delete
    @programme_group = ProgrammeGroup.find(params[:id])
    if @programme_group.courses.empty?
    @programme_group.update_attributes(:is_deleted=>true)
    flash[:notice]="Successfully deleted"
    respond_to do |format|
      format.html { redirect_to(programme_groups_url) }
      format.xml  { head :ok }
    end
    else
      flash[:notice]="Unable to delete. Dependency exists"
      redirect_to :action=>'index'
    end
  end


  def manage_programs
    program_group = ProgrammeGroup.find(params[:id])
    @courses = program_group.courses.active.paginate(:per_page=>30,:page=>params[:page])
  end


  def manage_user_groups
    @user = User.find_by_username(params[:id])
    if request.post?
      @user.programme_group_ids = params[:programme_group_ids]
      redirect_to :controller => :user, :action => :profile, :id=> @user.username
    end
  end

end
