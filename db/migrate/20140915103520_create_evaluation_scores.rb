class CreateEvaluationScores < ActiveRecord::Migration
  def self.up
    create_table :evaluation_scores do |t|
    	t.integer :student_id
    	t.integer :evaluation_pattern_id
    	t.integer :score
    	t.integer :weightage

      t.timestamps
    end
  end

  def self.down
    drop_table :evaluation_scores
  end
end
