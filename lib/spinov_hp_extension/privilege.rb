module SpinovHpExtension
module PrivilegeExt

     def self.included (base)

      base.instance_eval do
         alias_method :applicable?, :applicable_with_management
      end
    end



     def applicable_with_management
       role_plugin_hash = Authorization::Engine.instance.roles_plugin_hash[self.name.underscore.to_sym]
       s  =  role_plugin_hash.nil? ? false : (role_plugin_hash.include? nil) ? true : (role_plugin_hash & FedenaPlugin.accessible_plugins).present? ? true : false
       s ? management_applicable  : false
     end


     def management_applicable
       if ProgrammeGroup.management_roles.include?(self.name.underscore.to_sym)
         mr = ProgrammeGroup.management_roles
         token = false
         if Fedena.present_user.present?
           cp = mr & Fedena.present_user.role_symbols
           token = true if Privilege.find_all_by_name(cp.map{|x|x.to_s.camelize}, :conditions => "priority > #{self.priority} AND id != #{self.id}").present?
           token = true if Fedena.present_user.admin
         end
       else
         token = true
       end
       token
     end



end

end