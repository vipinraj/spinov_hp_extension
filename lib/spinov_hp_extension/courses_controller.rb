module SpinovHpExtension
  module CoursesController



    def self.included (base)
      base.instance_eval do
        alias_method_chain :new,:patched_new
        alias_method_chain :edit ,:patched_edit
        alias_method :update,:new_update
        alias_method :destroy,:new_destroy
        alias_method :create, :new_create

      end
    end


    def new_with_patched_new
      new_without_patched_new
      render :template=>"courses/patched_new"
    end


    def edit_with_patched_edit
      edit_without_patched_edit
      render :template=>"courses/patched_edit"

    end

    def new_update
     if @course.update_attributes(params[:course])
      flash[:notice] = "Course details updated successfully"
      redirect_to :action=>'manage_course'
    else
      @grade_types=Course.grading_types_as_options
      render 'courses/patched_edit'
    end
    end


  def new_destroy
    if @course.batches.active.empty?
      @course.inactivate
      flash[:notice]="Deleted Successfully"
      redirect_to :action=>'manage_course'
    else
      flash[:warn_notice]="Unable to Delete. Please remove existing batches and students."
      redirect_to :action=>'manage_course'
    end

  end

    def new_create
    @course = Course.new params[:course]
    if @course.save
      flash[:notice] = "#{t('flash1')}"
      redirect_to :action=>'manage_course'
    else
      @grade_types=Course.grading_types_as_options
    render 'courses/patched_new'
    end
    end





  end
end