require 'dispatcher'
require 'spinov_hp_extension/privilege'


module SpinovHpExtension
	def self.attach_overrides
    Dispatcher.to_prepare :spinov_hp_extension do
     ::Privilege.instance_eval {include PrivilegeExt}
    end
	end
end
