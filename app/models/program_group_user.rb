class ProgramGroupUser < ActiveRecord::Base
  belongs_to :user
  belongs_to :programme_group
end
