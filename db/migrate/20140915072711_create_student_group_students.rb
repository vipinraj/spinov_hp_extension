class CreateStudentGroupStudents < ActiveRecord::Migration
  def self.up
    create_table :student_group_students do |t|
    	t.integer :student_group_id
    	t.integer :student_ids

      t.timestamps
    end
  end

  def self.down
    drop_table :student_group_students
  end
end
