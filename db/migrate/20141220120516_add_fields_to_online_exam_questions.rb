class AddFieldsToOnlineExamQuestions < ActiveRecord::Migration
  def self.up
    add_column :online_exam_groups_questions, :negative_mark, :decimal, :precision=>15, :scale=>4
  end

  def self.down
  end
end
